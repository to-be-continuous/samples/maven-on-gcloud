#!/usr/bin/env bash

echo "[gcp-deploy] Deploy burger/${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}..."

# prepare GAE deployment directory
mkdir -p target/gae
cp target/*.jar target/gae

# copy manifest with envsubst
awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH-3);gsub("[$]{"var"}",ENVIRON[var])}}1' < src/main/appengine/app.yaml > target/gae/app.yaml

# gcloud deploy
# use $CI_ENVIRONMENT_SLUG as the version (to have several env coexist)
cd target/gae

if [[ "${environment_type}" == "production" ]]
then
  promote_opt="--promote"
else
  promote_opt="--no-promote"
fi

gcloud --quiet app deploy --project="${gcp_project_id}" --version="${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}" $promote_opt

# Retrieve deployed app URL
api_url=$(gcloud app browse --no-launch-browser --project="${gcp_project_id}" --service="burger" --version="${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}")

echo "API created/updated: $api_url"

# Finally set the dynamically generated env url
echo "$api_url" > environment_url.txt
