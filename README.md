# Maven on Google Cloud project sample

This project sample shows the usage of _to be continuous_ templates:

* Maven
* Google Cloud ([App Engine](https://cloud.google.com/appengine))
* Postman

The project exposes a basic JSON/Rest API, testable with Swagger UI.

It is a basic [Quarkus](https://quarkus.io/) application that uses an in-memory database (H2).

## Maven template features

This project uses the following features from the Maven template:

* Overrides the default Maven docker image by declaring the `$MAVEN_IMAGE` variable in the `.gitlab-ci.yml` file,
* Defines the `SONAR_HOST_URL` (enables SonarQube analysis),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `pom.xml`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable.

The Maven template also implements [unit tests report](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) and
[code coverage](https://docs.gitlab.com/ee/ci/testing/code_coverage.html) integration in GitLab.

## Google Cloud template features

This project uses Google [App Engine](https://cloud.google.com/appengine) to host the Java application.

This project uses the following features from the Google Cloud template:

* Defines mandatory `$GCP_KEY_FILE` in the project variables,
* Enables review environments by declaring the `$GCP_REVIEW_PROJECT` in the project variables,
* Enables staging environment by declaring the `$GCP_STAGING_PROJECT` in the project variables,
* Enables production environment by declaring the `$GCP_PROD_PROJECT` in the project variables.

The GitLab CI Google Cloud template also implements [environments integration](https://gitlab.com/to-be-continuous/samples/maven-on-gcloud/environments) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).

Lastly, the deployment script implements the [dynamic way](https://docs.gitlab.com/ee/ci/environments/#set-a-dynamic-environment-url) of
defining the environment URLs: retrieves the generated server URL with [`gcloud app browse`](https://cloud.google.com/sdk/gcloud/reference/app/browse) command, and dumps it into a `environment_url.txt` file, supported by the template.

### implementation details

In order to perform Google Cloud deployments, this project implements:

* `gcp-deploy.sh` script: generic deployment script using `gcloud app` commands,
* `gcp-cleanup.sh` script: generic cleanup script using `gcloud app` commands.

All those scripts and descriptors make use of variables dynamically evaluated and exposed by the Google Cloud template:

* `${environment_name}`: the application target name to use in this environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`)
* `${environment_type}`: the environment type (`review`, `integration`, `staging` or `production`)
* `${hostname}`: the environment hostname, extracted from `${CI_ENVIRONMENT_URL}` (has to be explicitly declared as [`environment:url`](https://docs.gitlab.com/ee/ci/yaml/#environmenturl) in your `.gitlab-ci.yml` file)
* `${gcp_project_id}`: the current Google Cloud project ID associated to your environment

They also use `${CI_ENVIRONMENT_SLUG}`, the [GitLab CI environment slug name](https://docs.gitlab.com/ee/ci/environments/#environment-variables-and-runners) used as the Google AppEngine version.

## Postman

This project also implements Postman acceptance tests, simply storing test collections in the [postman/](./postman) directory.
