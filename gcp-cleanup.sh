#!/usr/bin/env bash

echo "[gcp-cleanup] Cleanup burger/${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}..."

# use $CI_ENVIRONMENT_SLUG as the version (to have several env coexist)
gcloud --quiet app versions delete --project=${gcp_project_id} --service=burger ${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}
