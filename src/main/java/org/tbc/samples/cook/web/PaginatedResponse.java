package org.tbc.samples.cook.web;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import lombok.Value;

@Schema(description = "Paginated elements response")
@Value
public class PaginatedResponse<T> {
    @Schema(description = "Total number of elements")
    private final long totalElements;

    @Schema(description = "Total number of pages")
    private final long totalPages;

    @Schema(description = "Number of elements per pages")
    private final long perPage;

    @Schema(description = "Current response page")
    private final long page;

    @Schema(description = "Current page elements")
    private final List<T> elements;

    public PaginatedResponse(PanacheQuery<T> query) {
        this.totalElements = query.count();
        this.totalPages = query.pageCount();
        this.perPage = query.page().size;
        this.page = query.page().index;
        this.elements = query.list();
    }
}
